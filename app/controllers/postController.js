//Khơi tạo mongoose:
const mongoose = require('mongoose');
//import model:
const postModel = require('../models/postModel')

//Create new post:
const createNewPost = async (req, res) => {
    //thu thập dữ liệu:
    const userId = req.query.userId;
    const { title, body } = req.body;

    //validate dữ liệu

    if (!userId) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is required !"
        })
    }
    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "title is required !"
        })
    }
    if (!body) {
        return res.status(400).json({
            status: "Bad request",
            message: "body is required !"
        })
    }
    //thực thi model:
    const newPostCreateData = {
        _id: new mongoose.Types.ObjectId,
        userId,
        title,
        body
    }
    try {
        const newPostCreated = await postModel.create(newPostCreateData);
        if (newPostCreated) {
            return res.status(201).json({
                status: `Create new post of Post Id ${userId} successfully !`,
                data: newPostCreated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

//get all post:
const getAllPosts = async (req, res) => {
    const userId = req.query.userId
    try {
        if (userId) {
            const postList = await postModel.find({ userId: userId });
            if (postList && postList.length > 0) {
                return res.status(200).json({
                    status: "Get all posts successfully!",
                    data: postList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any posts",
                    data: postList

                })
            }
        }else{
            const postList = await postModel.find();
            if (postList && postList.length > 0) {
                return res.status(200).json({
                    status: "Get all posts successfully!",
                    data: postList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any posts",
                    data: postList

                })
            }
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get post by id:
const getPostById = async (req, res) => {
    const postId = req.params.postId;
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Post Id ${postId} is invalid`
        })
    }
    try {
        const postFoundById = await postModel.findById(postId);
        if (postFoundById) {
            return res.status(200).json({
                status: `Post found by Id ${postId} is successfully !`,
                data: postFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Posts by id ${postId}`,
                data: postFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update post by id
const updatePostById = async (req, res) => {
    const postId = req.params.postId;
    const { title, body } = req.body;
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Post Id ${postId} is invalid !`
        })
    }
    //validate:
    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "title is required !"
        })
    }
    if (!body) {
        return res.status(400).json({
            status: "Bad request",
            message: "body is required !"
        })
    }

    const postUpdateData = {
        title,
        body
    }
    try {
        const newPostUpdated = await postModel.findByIdAndUpdate(postId, postUpdateData);
        if (newPostUpdated) {
            return res.status(200).json({
                status: `Update Post by id ${postId} successfully !`,
                data: newPostUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any Posts by Id ${postId}`,
                data: newPostUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//delete Post by id
const deletePostById = async (req, res) => {
    const postId = req.params.postId;
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Post Id ${postId} is invalid !`
        })
    }
    try {
        const postDeleted = await postModel.findByIdAndDelete(postId)
        if (postDeleted) {
            return res.status(204).json({
                status: `Delete Post by id ${postId} successfully`,
                data: postDeleted
            })
        } else {
            return res.status(404).json({
                status: `Not found any Posts by Id ${postId}`,
                data: postDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get post of user:
const getPostOfUser = async (req, res) => {
    const userId = req.params.userId
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid !`
        })
    }
    try {
        const userPostList = await postModel.find({ userId: userId });
        if (userPostList && userPostList.length > 0) {
            return res.status(200).json({
                status: `Get post of user ${userId} successfully!`,
                data: userPostList
            })
        } else {
            return res.status(404).json({
                status: "Not found any posts",
                data: userPostList

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}


module.exports = { createNewPost, getAllPosts, getPostById, updatePostById, deletePostById, getPostOfUser }
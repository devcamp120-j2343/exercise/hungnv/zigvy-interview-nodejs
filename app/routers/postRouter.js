//Khai báo thư viện express:
const express = require('express');
const { createNewPost, getAllPosts, getPostById, updatePostById, deletePostById, getPostOfUser } = require('../controllers/postController');

//Khai báo  router:
const postRouter = express.Router();


postRouter.post("/posts", createNewPost)

postRouter.get("/posts", getAllPosts)

postRouter.get("/posts/:postId", getPostById)

postRouter.put("/posts/:postId", updatePostById)

postRouter.delete("/posts/:postId", deletePostById)

postRouter.get("/users/:userId/posts", getPostOfUser)


module.exports = { postRouter }
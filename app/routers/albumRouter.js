//Khai báo thư viện express:
const express = require('express');
const { createAlbum, getAllAlbums, getAlbumById, updateAlbumById, deleteAlbumById, getAlbumOfUser } = require('../controllers/albumController');

//Khai báo  router:
const albumRouter = express.Router();


albumRouter.post("/albums", createAlbum);

albumRouter.get("/albums", getAllAlbums);

albumRouter.get("/albums/:albumId", getAlbumById);

albumRouter.put("/albums/:albumId", updateAlbumById);

albumRouter.delete("/albums/:albumId", deleteAlbumById);

albumRouter.get("/users/:userId/albums", getAlbumOfUser);




module.exports = { albumRouter }